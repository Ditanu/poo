#include <iostream>
#include <string>
using namespace std;


long s = 0;

long& suma(int x, int y)
{
    long s = 0;
    s = x + y;
    return s;
}


class Masina
{
private:
    char* producator;
    string model;
    int viteza;
    const int serieSasiu;

public:
    Masina() : serieSasiu(1)
    {
        producator = NULL;
        model = "";
        viteza = 0;
    }

    Masina(int _viteza) :serieSasiu(0)
    {
        producator = NULL;
        model = "";
        viteza = _viteza;
    }

    Masina(const char* _producator, string _model, int _viteza) : serieSasiu(2)
    {
        model = _model;
        viteza = _viteza;
        if (_producator != NULL)
        {
            producator = new char[strlen(_producator) + 1];
            strcpy_s(producator, strlen(_producator) + 1, _producator);
        }
        else
        {
            producator = NULL;
        }
    }

    Masina(const Masina& m) : serieSasiu(m.serieSasiu)
    {
        model = m.model;
        viteza = m.viteza;
        if (m.producator != NULL)
        {
            producator = new char[strlen(m.producator) + 1];
            strcpy_s(producator, strlen(m.producator) + 1, m.producator);

        }
        else
        {
            producator = NULL;
        }
    }

    ~Masina()
    {
        if (producator != NULL)
        {
            delete[] producator;
            producator = NULL;
        }

       
    }

    Masina& operator = (const Masina& m) 
    {   
      if (this != &m)
      {
           if (producator != NULL)
        {
            delete[] producator;
            producator = NULL;
        }
      
        model = m.model;
        viteza = m.viteza;
        if (m.producator != NULL)
        {
            producator = new char[strlen(m.producator) + 1];
            strcpy_s(producator, strlen(m.producator) + 1, m.producator);

        }
       }
        return *this; // ca sa luam valoarea
    }

    void setViteza(int _viteza)
    {
        viteza = _viteza;
    }

    int getViteza()
    {
        return viteza;
    }

    char* getProducator()
    {
        if (this->producator != NULL)
        {
            char* copieProducator;
            copieProducator = new char[strlen(producator) + 1];
            for (int i = 0; i < strlen(producator) + 1; i++)
            {
                copieProducator[i] = producator[i];
            }
            return copieProducator;
        }
        
            return NULL;
    }

    void setProducator(const char* _producator)
    {
        if (_producator != NULL)
        {
            if (producator != NULL)
            {
                delete[] producator;
            }
            producator = new char[strlen(_producator) + 1];
            strcpy_s(producator, strlen(_producator) + 1, _producator);
        }
    }
   };


int main()
{
    Masina m1;
    Masina m2("Dacia", "Spring", 90);
    m1.setViteza(30);
    cout << m1.getViteza() << endl;
    m1.setProducator("Renault");
    cout << m1.getProducator() <<endl;
    char* prod = m2.getProducator(); // APEL CORECT DE GETTER CA SA NU FACEM MEMORY LEAK
    cout << prod << endl;
    delete[] prod;
    prod = NULL;

    Masina m0(100);
    Masina m = 100;
    Masina m3(m2);

    Masina m4 = m2; // constructor de copiere pentru ca copiez un obiect intr-un obiect gol pe care il cream
    m1 = m4;
    m1.operator=(m4);

    int x = 1;
    int y = 2;
    int z = y = x;
    cout << endl << z;
    m1 = m2 = m3;
    m1.operator=(m2.operator=(m3));
}
