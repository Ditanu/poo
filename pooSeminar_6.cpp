#include <iostream>
#include <string>

using namespace std;

class Bicicleta
{
private:
	string producator;
	float greutate;
	int nrViteze;
	int* distante = nullptr;
	int nrExcursii = 0;
	const int serie;
	static string taraProductie;
	
	

public:

	/*Bicicleta() {
		producator = "Necunoscut";
		greutate = 3;
		nrViteze = 1;
	}*/

	//constructor cu parametri ce joaca si rol de constructor default
	//constructor with parameters that works also as a default constructor
	Bicicleta(string _producator = "Necunoscut", float _greutate = 3):serie(1)
	{
		producator = _producator;
		greutate = _greutate;
		nrViteze = 1;
	}

	//initializare atribute in antet
	//attribute initialization in the constructor's header
	Bicicleta(string _producator, float _greutate, int nrViteze) : producator(_producator),
		greutate(_greutate), nrViteze(nrViteze), serie(2)
	{

	}

	Bicicleta(int serie, string producator, int* distante, int nrExcursii):serie(serie)
	{
		this->producator = producator;
		if (distante != nullptr && nrExcursii > 0) {
			this->distante = new int[nrExcursii];
			for (int i = 0; i < nrExcursii; i++)
			{
				this->distante[i] = distante[i];
			}
			this->nrExcursii = nrExcursii;
		}
	}

	//constructor de copiere
	//copy constructor
	// folosim & ca sa nu se copieze valoarea si const ca sa nu modificam valoarea
	Bicicleta(const Bicicleta& b):serie(b.serie)
	{
		producator = b.producator;
		nrViteze = b.nrViteze;
		greutate = b.greutate;
		if (b.distante != nullptr && b.nrExcursii > 0)
		{
			this->distante = new int[b.nrExcursii];
			for (int i = 0; i < b.nrExcursii; i++)
			{
				this->distante[i] = b.distante[i];
			}
			this->nrExcursii = b.nrExcursii;
		}

	}

	//destructor
	~Bicicleta()
	{
		if (distante != nullptr) {
			delete[] distante;
			distante = nullptr;
		}

	}

	//getter
	string getProducator() {
		return producator;
	}

	//setter
	void setProducator(string _producator) {
		if (_producator.length() > 0)
		{
			producator = _producator;
		}
	}

	float getGreutate() {
		return greutate;
	}
	void setGreutate(float _greutate) {
		if (_greutate > 0)
		{
			greutate = _greutate;
		}
	}

	int* getDistante() {

		if (distante != nullptr && nrExcursii > 0)
		{
			int* copie = new int[nrExcursii];
			for (int i = 0; i < nrExcursii; i++)
			{
				copie[i] = distante[i];
			}
			return copie;
		}
		else
			return nullptr;
	}

	int getNrExcursii()
	{
		return nrExcursii;
	}

	void setDistante(int* distante, int nrElemente)
	{
		if (distante != nullptr && nrExcursii != 0)
		{
			if (this->distante != nullptr)
			{
				delete[] this->distante;
			}
			this->distante = new int[nrExcursii];
			this->nrExcursii = nrExcursii;
			for (int i = 0; i < nrExcursii; i++)
			{
				this->distante[i] = distante[i];
			}
		}
	}


	int getDistanta(int index) {
		if (index >= 0 && index < nrExcursii)
		{
			return distante[index];
		}
		else
			return -1;
	}

	void setDistanta(int valoare, int index)
	{
		if (index >= 0 && index < nrExcursii)
		{
			distante[index] = valoare;
		}
	}

	static string getTaraProductie()
	{
		return taraProductie;
	}

	static void setTaraProductie(string taraProductie)
	{
		Bicicleta::taraProductie = taraProductie;
	}

	static long distantaTotala(Bicicleta* biciclete, int nrBiciclete)
	{
		long sumaDist = 0;
		if (biciclete != nullptr && nrBiciclete > 0)
		{
			for (int i = 0; i < nrBiciclete; i++)
			{	
				if (biciclete[i].distante != nullptr)
				{
					for (int j = 0; j < biciclete[i].nrExcursii; j++)
					{
						sumaDist = biciclete[i].distante[j] + sumaDist;
					}
				}
			}
		}

		return sumaDist;
	}

	void adaugaDistanta(int distanta)
	{
		if (nrExcursii < 0)
		{
			nrExcursii = 0;
		}

		int* copie = new int[nrExcursii + 1];
		if (distante != nullptr)
		{
			for (int i = 0; i < nrExcursii; i++)
			{
				copie[i] = distante[i];
			}

			delete[] distante;
		}
		copie[nrExcursii] = distanta;
		nrExcursii++;
		distante = copie;
	}
};

string Bicicleta::taraProductie = "Romania";

int main() {

	Bicicleta b1;
	Bicicleta b2("Pegas", 12);


	cout << b1.getProducator() << endl;
	b2.setProducator("RockRider");
	cout << b2.getProducator() << endl;
	cout << b2.getGreutate() << endl;

	//declararea unui vector de obiecte
	//va declansa apelul constructorului implicit
	//pentru fiecare obiect (element)
	//an array of objects will call the
	//default constructor for every element
	Bicicleta v[3];
	cout << v[0].getProducator() << endl;

	Bicicleta* pb;
	//alocare obiect in heap folosind ctor default
	//allocating an object in heap by using the defaul ctor
	pb = new Bicicleta();
	//object deallocation from heap
	//stergere obiect din heap
	delete pb;
	pb = nullptr;

	//alocare obiect in heap folosind ctor cu parametri
	//allocating an object in heap by using the ctor with parameters
	pb = new Bicicleta("B'twin", 14);
	cout << pb->getProducator() << endl;
	delete pb;
	pb = nullptr;

	Bicicleta b3("Pegas", 10, 18);
	int d[3] = { 5,2,7 };
	Bicicleta b4(123,"Romet", d, 3);
	d[0] = 99;

	//Apeluri explicite constructor de copiere
	//Explicit calls for the copy constructor
	Bicicleta b5 = b4;
	Bicicleta b6(b5);
	int* dist = b4.getDistante();
	dist[0] = 55;
	delete[] dist;

	int vector[] = { 11,7,9,13 };
	b4.setDistante(vector, 4);
	dist = b4.getDistante();
	for (int i = 0; i < b4.getNrExcursii(); i++)
	{
		cout <<dist[i] << endl;
		cout << b4.getDistanta(i);
	}
	delete[] dist;
	cout << endl;
	cout << b3.getTaraProductie() << endl;
	b2.setTaraProductie("Germania");
	cout << b4.getTaraProductie() << endl;
	cout << Bicicleta::getTaraProductie()<< endl;

	Bicicleta bic[] = { b4, b5 };
	cout << Bicicleta::distantaTotala(bic, 2) << endl;

	b4.adaugaDistanta(20);
	for (int i = 0; i < b4.getNrExcursii(); i++)
	{
		cout << b4.getDistanta(i) << endl;
	}
}

