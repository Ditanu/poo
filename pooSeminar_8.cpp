#pragma warning(disable : 4996) //permite folosirea strcpy in loc de strcpy_s
#include <iostream>
#include <string>
using namespace std;

long s = 0;

long& suma(int x, int y)
{
	s = x + y;
	return s;
}


class Roata {
private:
	int dimensiune;
	int nrSuruburi;
};

class Masina
{
private:
	class Motor {
	private:
		int putere;
		string producator;
	};
	char* producator;
	string model;
	int viteza;
	const int serieSasiu;
	Motor motor;
	Roata roti[4];
public:
	Masina() :serieSasiu(0)
	{
		producator = nullptr;
		model = "";
		viteza = 0;
	}
	Masina(const char* producator, string model, int viteza) :serieSasiu(1)
	{
		this->model = model;
		this->viteza = viteza;
		if (producator != nullptr)
		{
			this->producator = new char[strlen(producator) + 1];
			strcpy_s(this->producator, strlen(producator) + 1, producator);
		}
		else
		{
			this->producator = nullptr;
		}
	}
	void setViteza(int viteza)
	{
		if (viteza > 0)
		{
			this->viteza = viteza;
		}
	}
	int getViteza()
	{
		return this->viteza;
	}

	char* getProducator()
	{
		if (this->producator)
		{
			char* copie = new char[strlen(producator) + 1];
			strcpy_s(copie, strlen(producator) + 1, producator);
			return copie;
		}
		else return nullptr;

	}
	Masina(int _viteza) :serieSasiu(2)
	{
		producator = nullptr;
		model = "";
		viteza = _viteza;
	}
	Masina(const Masina& m) :serieSasiu(m.serieSasiu)
	{
		model = m.model;
		viteza = m.viteza;
		if (m.producator != nullptr)
		{
			producator = new char[strlen(m.producator) + 1];
			strcpy_s(producator, strlen(m.producator) + 1, m.producator);
		}
		else
		{
			producator = nullptr;
		}
	}
	void setProducator(const char* producator)
	{
		if (producator != nullptr)
		{
			if (this->producator != nullptr)
			{
				delete[] this->producator;
			}
			this->producator = new char[strlen(producator) + 1];
			strcpy_s(this->producator, strlen(producator) + 1, producator);
		}
	}

	Masina& operator=(const Masina& m)
	{
		if (this != &m)
		{
			if (producator != nullptr)
			{
				delete[] producator;
			}
			model = m.model;
			viteza = m.viteza;
			if (m.producator != nullptr)
			{
				producator = new char[strlen(m.producator) + 1];
				strcpy_s(producator, strlen(m.producator) + 1, m.producator);
			}
		}
		return *this;
	}

	bool operator!()
	{
		bool ok = this->viteza > 0 ? true : false;
		return ok;
	}

	//pre-incrementare
	Masina operator++ () {
		this->viteza++;
		return *this;
	}

	//post-incrementare
	Masina operator++ (int x)
	{
		Masina copie = *this;
		this->viteza++;
		return copie;
	}

	// operator +
	Masina operator+(const Masina& m)
	{
		Masina copie = *this;
		copie.viteza = this->viteza + m.viteza;
		return copie;
	}

	Masina operator+(int valoare)
	{
		Masina copie = *this;
		copie.viteza = copie.viteza + valoare;
		return copie;
	}


	~Masina()
	{
		if (producator != nullptr)
		{
			delete[] producator;
		}
		producator = nullptr;
	}
	friend Masina operator+(int, Masina);
	friend ostream& operator<<(ostream&, Masina);
	friend istream& operator>>(istream&, Masina&);
};

Masina operator+ (int valoare, Masina m)
{
	m.viteza += valoare;
	return m;
}

ostream& operator<<(ostream& out, Masina m)
{
	out << "producator: " << m.producator<<endl;
	out << "model: " << m.model<<endl;
	out << "viteza: " << m.viteza << endl;
	return out;
}

istream& operator>>(istream& in, Masina& m)
{
	string buffer;
	cout << "producator: ";
	in >> buffer;
	m.setProducator(buffer.c_str());

	cout << "model: ";
	in >> m.model;
	int v;
	cout << "viteza:";
	in >> v;
	m.setViteza(v);
	return in;

}

//bool operator!(Masina m)
//{
//	return m.getViteza() > 0;
//}



int main()
{
	Masina m1;
	Masina m2("Dacia", "Spring", 90);
	m1.setViteza(30);
	cout << m1.getViteza() << endl;
	m1.setProducator("Renault");
	cout << m1.getProducator() << endl;
	cout << m2.getProducator() << endl;
	char* prod = m2.getProducator();
	cout << prod << endl;
	delete[] prod;
	prod = nullptr;
	Masina m3(m2);
	Masina m0(100);
	Masina m = 100;
	Masina m4 = m2;
	m1 = m4;
	m1.operator=(m4);
	int x = 1;
	int y = 2;
	int z = y = x;
	m1 = m2 = m3;
	m1.operator=(m2.operator=(m3));

	if (!m4) {
		cout << "Masina se deplaseaza";
	}
	else
	{
		cout << "Masina nu se deplaseaza";
	}
	
	m4 = ++m2;

	m3 = m2++;

	Masina m5 = m1 + m3;
	Masina m6 = m2 + 15;
	Masina m7 = 9 + m3;
	cout << m7 << "Masina" << endl;
	Masina m8;
	cin >> m8;
	cout << m8 << endl;
}
